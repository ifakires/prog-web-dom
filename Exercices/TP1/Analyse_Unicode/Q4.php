<style>
<?php include 'Unicode.css'; ?>
</style>

<table>

<?php   
    echo "<tr>";
    for($x=64; $x<80; $x++){                //La boucle permet de récupérer le code ASCII de toutes les lettres de notre tableau
        echo "<td>".sprintf('%c',$x)."</td>";   //On affiche ces lettre via leur code ASCII(via %c)
    }
    echo "<tr>";
    for($y=64; $y<80; $y++){ //Cette boucle affiche les codepoint de ces lettres sous forme de lien cliquable
        $url = 'https://util.unicode.org/UnicodeJsps/character.jsp?a=00'.sprintf('%x',$y); //On récupere le lien d'une lettre sur le site unicode en le completant du bout d'url correspondant a notre lettre actuelle
        echo "<td>"."<a href=$url>".sprintf('U+00%x',$y)."</a>"."</td>"; //Création du lien via la balise <a>
    }

?>

</table>