<html>
  <head>
    <title>Analyse des Caractères Unicode</title>
    <meta charset="utf-8" />
  </head>
  <body>
    <form method="GET" action="">
      <input type="text" name="lettre" placeholder="Entrez une lettre" />
      <input type="submit" value="Valider"/>
    </form>
  </body>
</html>




<?php
$codepoint = mb_ord(mb_substr($_GET['lettre'],0,1),'UTF-8'); //mb_substr permet ici de récupérer la premiere lettre de l'input au cas ou l'utilisateur rentrerait plus d'un caractère comme demandé
echo "Le codepoint de la lettre ".$_GET['lettre']." est : "; //mb_ord récupere ici le codepoint de la lettre donnée pour le format UTF-8
echo sprintf('U+00%x',$codepoint);                           //Affichage du codepoint dans le format habituel
?>

