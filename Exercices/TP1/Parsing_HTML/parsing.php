<?php

// avec DOM

$html = file_get_contents($args[1]);

// On instance l'objet et on charge le site dans le dom
$dom = new DOMDocument();
@$dom->loadHTML($html);

// On recuperer tous les elements h2 et h3
$h2s = $dom->getElementsByTagName('h2');
$h3s = $dom->getElementsByTagName('h3');

// On affiche
$matches = array();

foreach($h2s as $node){
    $matches[$node->getLineNo()] = $dom->saveHtml($node);
}

foreach($h3s as $node){
    $matches[$node->getLineNo()] = $dom->saveHtml($node);
}

ksort($matches);

print_r($matches);



