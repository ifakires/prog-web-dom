<style>
<?php include 'AgendaV2.css'; ?>
</style>

<?php echo '<h1>'.'Agenda Web - Version 7 Colonnes'.'</h1>'; ?>

<table>

    <form method="post" action="AgendaWeb-V2.php">
        <input type = "number" name = "year" placeholder = "Year" />
        <input type = "number" name = "month" placeholder = "Month" />
        <input type = "number" name = "day_event" placeholder = "Event Day" />
        <input type = "text" name = "event" placeholder = "Event" />
        <input type="submit" />
    </form>

<?php
  
    $year = $_POST["year"];
    $month = $_POST["month"];
    $Nb_days = cal_days_in_month(CAL_GREGORIAN, $month, $year); //Récupération du nombre de jours dans le mois sélectionné

    echo "Month: ".$month.'-'.$year;

    for( $x = 0; $x<$Nb_days; $x++){           //Boucle sur tout les jours du mois en cours
        $date = $year.'-'.$month.'-'.($x+1);   //On crée une string utilisable de la date au bon format
        $unixTimestamp = strtotime($date);     //Création d'une timespamp pour utiliser la fonction date 
        $dayOfWeek = date("l", $unixTimestamp); //La fonction nous renvoie sous forme de String le nom du jour pour une date donnée

        if($dayOfWeek == "Monday") {           //Si on est Lundi alors on écrit sur une nouvelle ligne(semaine) 
            echo '<br>';
            echo "<tr>";
        }
        else if($x == 0){                       //Nouvelle ligne au tout début de l'exécution
            echo "<tr>";
        }

        
        $info ="";                  
        if(($x+1) == $_POST["day_event"]){      //Si on arrive au jour de l'événement alors $info prend le texte rentré en parametre
            $info = $_POST["event"];
        }

        if($info == ""){
            echo '<td>'.$dayOfWeek.'</td>';
        }
        else{
        echo '<td>'.$dayOfWeek."<br>"."------"."<br>".$info.'</td>'; //Affiche le nom du jour puis l'événement inscrit dans le calendrier
        }
    }




  
?>

</table>