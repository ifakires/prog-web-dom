# Compte-rendu de TP

Sujet choisi : TP1

## Participants 

* Sami Ifakiren
* Guirguis Mirette
* Clément Nguyen

### Calcul d'intérêts composés

Pas de difficultés particulier, nous avons défini une fonction "cumul" dans le fichier libcalcul.php, et nous l'appelons dans le fichier resultat.php à l'aide des données du formulaire grâce à la méthode POST.

### Un peu de style CSS

Nous avons amélioré l'affichage HTML en ajoutant un cadre autour de la table et rajoutant quelques couleurs, nous pouvons nous aider de l'inspecteur du navigateur pour trouver comment sont organisés les éléments.

### La table de multiplication

On utilise une class appelée surlignee dans le fichier .css pour surligner la ligne de la table que l’on veut. Le numéro de cette ligne devra être passé en paramètre dans l’url ($_GET) et sera stockée dans la variable $line. Pour le résultat des multiplications on effectue simplement une boucle for sur la variable $x.

### Analyse des Caractères Unicode

Pour mener à bien cet exercice nous utiliserons une première boucle for sur la variable $x allant du code ASCII de ‘@’ a celui de ‘O’. Et une seconde boucle for qui permettra de récupérer et afficher dans les cellules correspondantes le codepoint en lien cliquable de chacun de ces caractères. Chaque lien renverra l’utilisateur sur la page du site unicode correspondante. 

### Calendrier - agenda web

Dans cet exercice nous allons utiliser la méthode $_POST afin de récupérer les données essentielles à l'affichage de l’agenda dans un premier temps (ce qui explique la présence d'erreurs lors du premier lancement de la page car ces paramètres n’ont pas encore été choisis). Ces paramètres sont l’année et le mois ( on rajoute pas la suite le numéro du jour de l’événement que l'on veut ajouter ainsi que le nom de cet événement).
Ici le mois se sélectionne uniquement en utilisant le numéro correspondant.
Nous faisons également usage de la fonction date() qui nous a permis de récupérer le nom d’un jour en fonction d’une date ce qui est très utile pour permettre un bon affichage et un retour à la ligne lorsque la cellule d’un Lundi doit être affichée.


### Analyse (parsing) d'un document HTML

La première version de parseur utilise DOM de PHP, nous récupérons en argument le lien du site dans lequel nous voulons parser. Ensuite, nous instancions DOM et nous appelons la méthode loadHTML(), qui va charger du code HTML. Ensuite, nous voulons récupérer tous les éléments 'h2', il suffit d'appeler la méthode prévu pour cela : getElementsByTagName('h2'), qui va chercher tous les éléments qui ont le nom de la balise locale donné, nous faisons pareil pour les balises h3, et nous affichons tout cela dans l'ordre.
La deuxième version fait la même chose mais nous n'utilisons pas DOM. Nous devons utiliser des expressions régulères à la place ce qui n'est pas très efficace car HTML ne peut pas être analyser par REGEX. 
