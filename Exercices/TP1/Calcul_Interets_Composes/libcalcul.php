<?php
function cumul($somme, $taux, $duree)
{
    $res = $somme * (1 + $taux / 100) ** $duree;
    return $res;
}
