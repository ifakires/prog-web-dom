<style>
<?php include 'Multiplication.css'; ?>
</style>

<table>

<?php

$row = $_GET["a"];
$lines = $_GET["b"];
$line = $_GET["ligne"];

for ($x = 1; $x<=$row; $x++){

    if($x == $line){
        echo '<tr class="surlignee" >'; //La class surlignee est crée dans le fichier Multiplciation.css et nous permet de surligner la ligne dont le numéro est $line
    }
    else{                               //Si nous ne sommes pas à la ligne a surligner
        echo "<tr>";
    }

    for($y = 1; $y<=$lines; $y++ ){

        echo "<td>".$x*$y."</td>";      //Affichage de la cellule contenant le résultat du produit
        
    }


}

?>

</table>