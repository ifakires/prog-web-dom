<?php
require_once("tp2-helpers.php");
$n = $_POST['n'];
$long = $_POST['long'];
$lan = $_POST['lan'];

$fichier = file("point_dacces.csv");
$array;
$i = 0;
foreach ($fichier as $value) {
	$ligne = str_getcsv($value);
	$nom = $ligne[0];
	$adresse = $ligne[1];
	$long = $ligne[2];
	$lat = $ligne[3];
	$array[$i] = ['nom' => $nom, 'adresse' => $adresse, 'long' => $long, 'lat' => $lat];
	$i++;
}
$centre = geopoint($long, $lat);
$i = 0;
$nom_distance;
foreach ($array as $point) {
	$point2 = geopoint($point['long'], $point['lat']);
	$distance = distance($centre, $point2);
	$array[$i] = ['nom' => $point['nom'], 'adresse' => $point['adresse'], 'long' => $point['long'], 'lat' => $point['lat'], 'distance' => $distance];
	$i++;
}
$distance = array_column($array, 'distance');
array_multisort($distance, SORT_ASC, $array);
$i = 0;

while ($i < $n) {
	$plus_proche[$i] = $array[$i];
	$i++;
}
$i = 0;

foreach ($plus_proche as $point) {
	$lon = $point['long'];
	$lat = $point['lat'];
	$url = "https://api-adresse.data.gouv.fr/reverse/?lon=$lon&lat=$lat";
	$json = smartcurl($url, 0);
	//echo mb_detect_encoding($json);
	$json = mb_convert_encoding($json, 'UTF-8', 'ASCII');
	$data = json_decode($json);
	while (is_null($data)) {
		$json = smartcurl($url, 0);
		$json = mb_convert_encoding($json, 'UTF-8', 'ASCII');
		$data = json_decode($json);
	}

	$adresse = $data->features[0]->properties->label;

	$plus_proche[$i] = ['nom' => $plus_proche[$i]['nom'], 'adresse' => $plus_proche[$i]['adresse'], 'long' => $plus_proche[$i]['long'], 'lat' => $plus_proche[$i]['lat'], 'adresse2' => $adresse];
	$i++;
}

print_r(json_encode($plus_proche, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
