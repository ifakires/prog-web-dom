<?php

// fonction qui trie un tableau de maniere croissante en fonction d'une cle
function val_sort($array, $key)
{
    foreach ($array as $k => $v) {
        $b[] = strtolower($v[$key]);
        asort($b);
    }
    foreach ($b as $k => $v) {
        $c[] = $array[$k];
    }
    return $c;
}

require_once("tp2-helpers.php");

// On lit le fichier json
$fileJson = "borneswifi_EPSG4326.json";
$jsonData = file_get_contents($fileJson);
$datas = json_decode($jsonData, true);

$points = array();

// Coordonnes de l'utilisateur
$maLon = $_GET['lon'];
$maLat = $_GET['lat'];
$monTop = $_GET['top'];

// On met dans points toutes les donnees bien organisees de notre Json
foreach ($datas['features'] as $data) {

    $points[] = array(
        'name' => $data['properties']['AP_ANTENNE1'],
        'adr' => $data['properties']['Antenne 1'],
        'lon' => $data['properties']['longitude'],
        'lat' => $data['properties']['latitude'],
        'dist' => distance(array(
            'lon' => $maLon,
            'lat' => $maLat
        ), array(
            'lon' => $data['properties']['longitude'],
            'lat' => $data['properties']['latitude']
        ))
    );
}

// On trie notre tableau points en fonction de la distance
$points = val_sort($points, 'dist');

// On recupere les adresses grace a l'API
$points_proches = array();
for ($i = 0; $i < $monTop; $i++) {

    $pt = &$points[$i];
    $api_url = "https://api-adresse.data.gouv.fr/reverse/?lon=" . $pt['lon'] . "&lat=" . $pt['lat'];
    $curl = smartcurl($api_url, 0);
    $dataCurl = json_decode($curl, true);
    foreach ($dataCurl['features'] as $val) {
        $pt['adr'] = $val['properties']['name'];
    }
    $points_proches[] = $points[$i];
}

// On affiche les points les plus proches
// print_r($points_proches); (question 9)

// Question 10

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Webservice</title>
</head>

<body>

    <table class="table">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Lieu</th>
                <th>Distance</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($points_proches as $pt) {
                echo "<tr>";
                echo "<td>" . $pt['name'] . "</td>";
                echo "<td>" . $pt['adr'] . "</td>";
                echo "<td>" . $pt['dist'] . "</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>