% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : TP2

## Participants 

* Sami Ifakiren
* Mirette Guirguis
* Clément Nguyen

### Filtres UNIX

1) Pour avoir le nombre de lignes dans le fichier il nous suffit d’exécuter la commande wc -l Data-Wifi.csv qui nous renvoie 69. Or la première ligne contient le nom des colonnes donc nous avons 68 points d'accès.

2) Ici nous allons exécuter la commande sort -u -t, -k2,2 Data-Wifi.csv | wc -1 qui nous renvoie 59 auquel on enlève encore 1 ce qui nous donne donc 58 localisations différentes.
La commande cut -d, -f2 Data-Wifi.csv | uniq -c | sort -r | head -n 1 nous permet 
de déterminer que c’est la Bibliothèque d’étude qui dispose du plus grand nombre	de points d'accès (il y en a 5).

### Traitements PHP

3) Pour compter le nombre de points d'accès, il suffit de récupérer notre fichier .csv à l'aide de la fonction file(), qui nous permet de renvoyer un tableau contenant les données de notre csv. Puis, nous devons boucler sur ce tableau afin de compter le nombre de lignes.

4) Nous devons créer une structure de données pratique contenant les données du fichier csv, cette structure est un tableau associatif de clés name, adr, lon et lat, pour créer ce dernier, nous devons utiliser la fonction str_csv et remplacer les clés par défaut, qui sont des chiffres (0,1,2 et 3) apar des noms plus parlant : name, adr, lon et lat.

5) On se place au centre de la place Grenette, grâce à la fonction geopoint(), puis à l'aide de notre structure précédemment créer, nous faisons des opérations afin de d'obtenir les points d'accès les plus proches du centre à moins de 200 mètres, ainsi que le point le plus proche.

6) Pour cette question, il nous a été demander de chercher les N points les plus proches de nous (place Grenette). Pour faire cela on avait besoin tout d’abord de calculer la distance entre tous les points d'accès WI-FI et la place grenette à travers la fonction distance qui nous a été donné, puis on a fait une structure qui est formée d’une liste associative et qui a pour chaque élément un nom et sa distance par rapport à la place grenette. Ensuite on a trié cette liste suivant la distance en utilisant la fonction array_multisort et pour indiquer qu'on veut trier par rapport à la distance, on a créé un column vecteur à travers la fonction array sort, que l'on a ajouté comme paramètre à la fonction array_multisort. Après en avoir trié la liste on fait une simple boucle pour afficher les n premiers points d'accès WI-FI.

7) Dans cette question il nous était demandé d’ajouter à notre structure les adresses précises des points d'accès wifi bien qu’elles ne soient pas fournies dans le fichier csv qu’on avait récupéré. L'idée était de faire un API classique à travers la fonction smart curl qui nous a été donnée. Cette fonction prend en argument l’url (qui comprend la longitude et la latitude de la place dont on veut avoir l’adresse) et elle nous retourne un string qui est un json string. Ce json string comprend toutes les informations concernant cette place. On veut maintenant récupérer une certaine information de cette chaîne de caractères que l’on vient de récupérer alors on a fait un décodage json pour transformer cette chaîne de caractères en un objet json et accéder à l’adresse demandée . Il est à noter qu’on fait la demande API avec une boucle car cette demande n’aboutit pas toujours alors on a fait ça pour nous assurer que notre programme marche toujours.

8) Dans cette fonction, il nous est demandé de modifier le travail qu’on avait fait pour que cela soit un web service un prend en url la longitude et la latitude d’une place quelconque et le web service nous affiche les informations qu’il sait à propos de N plus proche point d'accès . C’est quasiment le même travail que la fonction précédente avec une petite différence qu’on calcule la distance entre les points d'accès et la place de notre choix (dans la question précédente, on calculait la distance entre les points d'accès et la place grenette) et aussi on rend le résultat sous forme d’un json à travers la fonction json_encode.

9) Pour cette question, nous devons lire notre fichier json, nous avons utilisés la fonction file_get_contents() ainsi que json_decode() pour effectuer cela. Ensuite on réorganise bien les données obtenues dans un tableau nommé "points", dans ce tableau, nous mettons le nom, l'adresse, la longitude, la latitude, ainsi que la distance entre les points et notre point. Puis nous trions notre tableau en fonction de la distance de manière croissante. Nous récupérons des informations sur les adresses grâce à l'API du gouvernement et enfin nous pouvons afficher les points les plus proches.

10) Pour créer le client, nous avons écrit un formulaire dans le fichier ClientWebService.php, et il y a 3 champs : latitude, longitude et le nombre de points que l'utilisateur veut afficher. Ensuite, nous récupérons ces informations à l'aide de la méthode GET, et dans le fichier de la question précédente on traite ces informations afin de récupérer les points les plus proches et on affiche dans un tableau des informations sur ces derniers.

### Antennes GSM

1) Les informations que ce jeu de données contient en plus sont : Noms des opérateurs, la compatibilité 2G/3G/4G, identifiant de l’adresse, ID de l’antenne, Microcell, Technologie de l’antenne, numéro cartoradio et enfin le numéro de support. Ces informations peuvent être très utiles par exemple dans le cas d’une étude sur la couverture 4G comparé aux anciennes technologies. Dans notre code nous allons tout d’abord utiliser la fonction file pour rendre le fichier csv exploitable. Puis nous allons incrémenter une variable comptant le nombre d’antennes a la lecture de chaque ligne. Après avoir enlevé 1 au résultat final ( on enlève la première ligne du tableau) on obtient le nombre d’antennes qui pourra s’afficher.

2) Il y a 4 opérateurs en tout : Free, Orange, Bouygues et SFR. Bouygues a 26 antennes, SFR en a 30, Orange dispose de 26 antennes et enfin Free en
a 18 dans la zone. Dans notre fichier php nous avons effectué la démarche suivante pour récupérer ces informations. Après utilisation de la fonction explode nous créons un nouveau tableau appelé matrice ou nous allons ranger les colonnes qui nous sont utiles à savoir les ID des antennes et le nom de l’opérateur correspondant. Nous affectons à ces données 2 clés : “ant_id” et “Opérateur”. Il nous suffit ensuite de parcourir notre nouveau tableau et de compter les antennes dont chaque opérateur dispose.

3) Pour vérifier la validation de ce fichier, nous utilisons l’outil xmllint. Ce dernier a validé le fichier, en effet il ne retourne pas d’erreur de parseur.

4) Le fichier kml n’est pas très lisible en version “texte”, il est très compact et très long, avec les mêmes lignes répétées plusieurs fois. Cependant, lorsque nous ouvrons le fichier .kml avec Google Earth, toutes les antennes sont facilement observables et les informations concernant chacune sont facilement accessibles.

5) Le principe de notre code est de rendre le fichier JSON exploitable dans un premier temps via la fonction json_decode puis de récupérer dans un tableau appelé distances toutes les distances séparant le point sélectionné par l’utilisateur des différentes antennes. Nous récupérons également dans un tableau appelé opérateur ant toutes les features de l’antenne correspondante à la distance qui vient d'être calculée. Nous allons ensuite trier les distances dans l’ordre croissant et appliquer les mêmes modifications au tableau contenant les antennes correspondantes. Il ne nous reste plus qu'à afficher les antennes de la plus proche à la plus grande jusqu'à ce que le top sélectionné soit atteint.
