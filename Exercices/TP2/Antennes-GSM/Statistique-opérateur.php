<?php

$lines = file('Données-GSM.csv');
$matrice = array();

foreach($lines as $value){
    $array = explode(';',$value);

    $tab = ["ant_id"=>$array[0],"Opérateur"=>$array[3]];

    array_push($matrice,$tab);
}

$SFR = 0;
$BYG = 0;
$FREE = 0;
$ORA = 0;

foreach($matrice as $data){
    if($data["Opérateur"] == "SFR"){
        $SFR++;
    }
    if($data["Opérateur"] == "BYG"){
        $BYG++;
    }
    if($data["Opérateur"] == "FREE"){
        $FREE++;
    }
    if($data["Opérateur"] == "ORA"){
        $ORA++;
    }
}

echo "SFR dispose de :".$SFR." antennes";
echo "<br>";
echo "FREE dispose de :".$FREE." antennes";
echo "<br>";
echo "BOUYGUE TELECOM dispose de :".$BYG." antennes";
echo "<br>";
echo "ORANGE dispose de :".$ORA." antennes";


?>