<style>
<?php include 'TopN.css'; ?>
</style>

<?php echo '<h1>'.'Top N Opérateurs'.'</h1>'; ?>

    <form method="post" action="TopN.php">
        <input type = "text" name = "operateur" placeholder = "Opérateur" />
        <input type = "text" name = "top" placeholder = "Top" />
        <input type = "text" name = "lon" placeholder = "Lon" />
        <input type = "text" name = "lat" placeholder = "Lat" />
        <input type="submit" />
    </form>


<?php
    require_once("tp2-helpers.php");

    $fichier = json_decode(file_get_contents("DSPE_ANT_GSM_EPSG4326.JSON"));

    $operateur = $_POST["operateur"];
    $top = $_POST["top"];
    $lon = $_POST["lon"];
    $lat = $_POST["lat"];
    
    $location = geopoint($lon, $lat);

    $operateur_ant = array();
    $distances = array();

    foreach($fichier->features as $value){
        $op = $value->properties->OPERATEUR;
        if ($op==$operateur) {
            $point = geopoint($value->geometry->coordinates['0'], $value->geometry->coordinates['1']);
            array_push($distances, distance($point, $location));
            array_push($operateur_ant, $value);
        }
    }

    array_multisort($distances, SORT_ASC, $operateur_ant);


    echo "Voici les ID du Top ".$top." des antennes ".$operateur." les plus proches de la position choisie :";
    echo "<br>";

    $counter = 0;
    $already_displayed = array();
    foreach($operateur_ant as $data){
        if($counter == $top){
            break;
        }
        echo $data->properties->ANT_ID."<br>";
        array_push($already_displayed, $data->properties);
        $counter++;
    }
?>
