
<?php
require_once("tp2-helpers.php");


$fichier = file("Data-Wifi.csv");
$array;
$i = 0;
foreach ($fichier as $value) {
	$ligne = str_getcsv($value);
	$nom = $ligne[0];
	$adresse = $ligne[1];
	$long = $ligne[2];
	$lat = $ligne[3];
	$array[$i] = ['nom' => $nom, 'adresse' => $adresse, 'long' => $long, 'lat' => $lat];
	$i++;
}
$centre = geopoint(5.72752, 45.19102);
$i = 0;
//question 5
//liste des noms des points d'acces avec leur distance au point centre
$nom_distance;
foreach ($array as $point) {
	$point2 = geopoint($point['long'], $point['lat']);
	$distance = distance($centre, $point2);
	$nom_distance[$i] = ['nom' => $point['nom'], 'distance' => $distance];
	$i++;
}
// filter les points d'acces pour avoir les points d'acces qui sont moins de 200 metres
$i = 0;
foreach ($nom_distance as $point) {
	if ($point['distance'] <= 200) {
		$proche[$i] = $point;
		$i++;
	}
}
//la structure
//print_r($array);
//les plus proches du centre
//print_r($proche);
// trier la liste des points d'acces suivant la distance
$distance = array_column($nom_distance, 'distance');
//les donnes tries par distance
array_multisort($distance, SORT_ASC, $nom_distance);
//print_r($nom_distance);
//question 6
//afficher les n plus proches points d'acces relatifs a la place grenette
if (isset($argv[1])) {
	$i = 0;

	while ($i < $argv[1]) {
		$plus_proche[$i] = $nom_distance[$i];
		$i++;
	}
	//print_r($plus_proche);

}
$i = 0;
//ajouter l'adresse avec le sercice API
foreach ($array as $point) {
	$lon = $point['long'];
	$lat = $point['lat'];
	$url = "https://api-adresse.data.gouv.fr/reverse/?lon=$lon&lat=$lat";
	$json = smartcurl($url, 0);
	//echo mb_detect_encoding($json);
	$json = mb_convert_encoding($json, 'UTF-8', 'ASCII');
	$data = json_decode($json);
	while (is_null($data)) {
		$json = smartcurl($url, 0);
		$json = mb_convert_encoding($json, 'UTF-8', 'ASCII');
		$data = json_decode($json);
	}



	$adresse = $data->features[0]->properties->label;

	$array[$i] = ['nom' => $array[$i]['nom'], 'adresse' => $adresse, 'long' => $array[$i]['long'], 'lat' => $array[$i]['lat']];
	$i++;
}
print_r($array);


?>
