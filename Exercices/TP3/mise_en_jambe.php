<?php

include('tp3-helpers.php');

// on recupere l'id par le biais du formulaire
if (isset($_POST['id_film'])) {
  $id_film = $_POST['id_film'];
  // on se connecte a l'api 
  $url_component = "movie/" . $id_film;
  $params = null;
  $content = tmdbget($url_component, $params);

  // tableau contenant les infos 
  $content_array = json_decode($content, true);
} else {
  echo "Veuillez renseigner un identifiant de film !";
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Info film</title>
</head>

<body>
  <form method="POST" action="">
    <input type="text" name="id_film" placeholder="ID du film" />
    <button type="submit">Valider</button>
  </form>
  <?php if (isset($id_film)) { ?>
    <p>Titre : <?php echo $content_array['title']; ?></p>
    <p>Titre original : <?php echo $content_array['original_title']; ?></p>
    <p>Tagline : <?php
                  if ($content_array['tagline'] == "") {
                    echo "N/A";
                  } else {
                    echo $content_array['tagline'];
                  }
                  ?></p>
    <p>Description : <?php echo $content_array['overview']; ?></p>
    <p>Page TNDB : <a href="https://www.themoviedb.org/movie/<?php echo $id_film ?>"> Lien vers sa page publique</a></p>
  <?php } else {
    echo "";
  } ?>

</body>

</html>