% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : TP3 TMDB

## Participants 

* Sami IFAKIREN
* Clément NGUYEN

### Mise en jambes

1) La réponse donnée est en format JSON. Il s'agit du film "Fight Club". Le paramètre nous permet d'avoir les caractéristiques du film nottament, sa description ou bien son affiche en français.

2) En appelant l'API avec curl, nous obtenant la même réponse que la précédente. Nous avons crée un programme php minimal "exploration_cli.php" qui inclu en fichier header "tp3_helpers.php" et qui utilise les fonctions de ce dernier. On utilise d'abord la fonction tmdbget() pour obtenir la réponse de l'api, puis nous décodons grâce à json_decode() la réponse pour la stocker dans un tableau. la première fonction attend comme paramètre le chemin relatif du film soit "movie/550" et comme second rien car nous n'avons pas besoin de préciser des paramètres. Enfin, on affiche le tableau.

3) La page de détail se lance grâce au fichier "mise_en_jambe.php". Cette page comporte un champ texte qui attend l'identifiant du film duquel on veut obtenir des informations, nous avons choisi la méthode POST pour effectuer cela. Lorsqu'on a l'identifiant du film, nous pouvons lancer une requête à l'API avec l'identifiant. On décode la réponse et on stocke dans un tableau. Puis nous affichons les informations sur la même page.

### Les choses sérieuses

4) Il nous a été demandé d'avoir 3 versions du film : originale, française et anglaise, il nous a donc fallu appelé 3 fois l'API avec comme paramètre la langue. Ensuite nous décodons tout cela et nous pouvons afficher les informations dans la partie HTML.

5) En ce qui concerne l'affiche, il fallait trouver l'adresse de l'image qui est composé d'une base url, de la taille et du chemin qui se trouve dans la réponse de l'API. Enfin cette adresse sera renseigné dans l'attribut "src" de la balise "img".

6) Cette question se trouve dans le fichier "collection.php". Nous voulons, cette fois-ci le nom d'une collection dont l'utilisateur rentrera dans le champ vide. Pour obtenir la collection il faut appeler l'API de la même manière que précédemment mais changer l'url du chemin relatif (premier paramètre de tmdbget()) par "search/collection". On stocke ensuite la réponse dans un tableau grâce à json_decode() et on vérifie s'il y a un résultat. Ensuite nous pouvons afficher l'ensemble des films car nous avons l'id de la collection. Et donc il nous faut appelé encore une fois tmdget() avec comme chemin relatif "collection/id_collection" où id_collection se trouve dans le premier tableau. On obtient ensuite l'ensemble des films avec quelques informations notamment l'id de chaque film, le titre et la date de sortie. On affiche ces derniers sur la page.

7) On affiche tous les acteurs en ajoutant au chemin relatif "/credits", nous avons décidé de créer un tableau qui stocke tous les acteurs en faisant attention qu'il n'y ait pas de doublons. On vérifie donc cela grâce à la fonction in_array(). Puis nous affichons sur la page la liste des acteurs en appelant le tableau.

8) Il est possible d'afficher tous les acteurs qui jouent des hobbits. On regarde dans le tableau de la question précédente contenant tous les acteurs ainsi que leurs "characters", si ce champ comporte "Hobbit", "Frodo", "Bilbo", "Pippin", "Merry", alors ce sont des acteurs qui jouent des hobbits et on stocke leurs noms dans un tableau nommé "liste_acteur_hobbit".

9) Le lien-rebond appellera le fichier "traitement_q9". Nous passons en paramètre de ce lien l'id de l'acteur que l'on récupère dans ce fichier afin d'appeler l'API avec cet ID et enfin, obtenir tous les films dans lequel il a joué. On change le chemin relatif par : "person/id_acteur/movie_credits".

10) La dernière question est plutôt simple : dans le fichier "chose_serieuse.php", on appelle l'API avec comme chemin relatif "movie/id_film/videos", ensuite nous décodons la réponse. Dans le tableau il y a un champ "type" qui nous permet de savoir si c'est un trailer ou non, on effectue une condition pour vérifier si c'est un trailer, si oui on crée une variable "link" qui est composé du lien youtube et de la clé de la video que l'on trouve dans le champ "key" du tableau. Enfin avec ce lien, nous pouvons, dans la partie HTML, mettre le lien dans l'attribut "src" de la balise "iframe".




