<?php

include('tp3-helpers.php');

// on recupere l'id par le biais du formulaire
if (isset($_POST['id_film'])) {
    $id_film = $_POST['id_film'];

    $url_component = "movie/" . $id_film;

    // requetes 3 langues
    $content_og = tmdbget($url_component, null);
    $content_fr = tmdbget($url_component, ['language' => 'fr']);
    $content_en = tmdbget($url_component, ['language' => 'en']);

    // tableaux des 3 langues
    $content_array_og = json_decode($content_og, true);
    $content_array_fr = json_decode($content_fr, true);
    $content_array_en = json_decode($content_en, true);

    // traitement image
    $base_url = "https://image.tmdb.org/t/p/";
    $size = "w500";
    $file_path_og = $content_array_og["poster_path"];
    $file_path_fr = $content_array_fr["poster_path"];
    $file_path_en = $content_array_en["poster_path"];

    // src image
    $src_image_og = $base_url . $size . $file_path_og;
    $src_image_fr = $base_url . $size . $file_path_fr;
    $src_image_en = $base_url . $size . $file_path_en;

    // question 10 : bande annonce
    $url_component_video = "movie/" . $id_film . "/videos";
    $content_video = tmdbget($url_component_video, null);
    $content_video_array = json_decode($content_video, true);
    foreach ($content_video_array["results"] as $video) {
        if ($video["type"] == "Trailer") {
            $code_video = $video["key"];
            $link = "https://www.youtube.com/embed/" . $code_video;
            break;
        }
    }
} else {
    echo "Veuillez renseigner un identifiant de film !";
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="style.css" rel="stylesheet">
    <title>Utilisation de TMDB</title>
</head>

<body>
    <form method="POST" action="">
        <input type="text" name="id_film" placeholder="ID du film" />
        <button type="submit">Valider</button>
    </form>

    <h1>Info du film "<?php echo $content_array_fr['title'] . " (" . $content_array_og['title'] . ")" ?>"</h1>
    <table style="width:100%">
        <tr>
            <th class="info">Infos</th>
            <th>Version OG</th>
            <th>Version EN</th>
            <th>Version FR</th>
        </tr>
        <tr>
            <td class="info">Titre</td>
            <td><?php echo $content_array_og['title'] ?></td>
            <td><?php echo $content_array_en['title'] ?></td>
            <td><?php echo $content_array_fr['title'] ?></td>
        </tr>
        <tr>
            <td class="info">Titre original</td>
            <td><?php echo $content_array_og['original_title'] ?></td>
            <td><?php echo $content_array_en['original_title'] ?></td>
            <td><?php echo $content_array_fr['original_title'] ?></td>
        </tr>
        <tr>
            <td class="info">Tagline</td>
            <td><?php echo $content_array_og['tagline'] ?></td>
            <td><?php echo $content_array_en['tagline'] ?></td>
            <td><?php echo $content_array_fr['tagline'] ?></td>
        </tr>
        <tr>
            <td class="info">Description</td>
            <td><?php echo $content_array_og['overview'] ?></td>
            <td><?php echo $content_array_en['overview'] ?></td>
            <td><?php echo $content_array_fr['overview'] ?></td>
        </tr>
        <tr>
            <td class="info">Page TMDB</td>
            <td><a href="https://www.themoviedb.org/movie/<?php echo $id_film . "?language=en-US" ?>"> TMDB Link</a></td>
            <td><a href="https://www.themoviedb.org/movie/<?php echo $id_film . "?language=en-US" ?>"> TMDB Link</a></td>
            <td><a href="https://www.themoviedb.org/movie/<?php echo $id_film . "?language=fr-FR" ?>"> TMDB Link</a></td>
        </tr>
        <tr>
            <td class="info">Image</td>
            <td><img src="<?php echo $src_image_og ?>" alt="Poster OG"></td>
            <td><img src="<?php echo $src_image_en ?>" alt="Poster OG"></td>
            <td><img src="<?php echo $src_image_fr ?>" alt="Poster OG"></td>
        </tr>
    </table>

    <!-- Question 10 : bande annonce du film -->
    <iframe width="420" height="315" src="<?php echo $link ?>">
    </iframe>

</body>

</html>